#
# Variables Configuration
#

variable "cluster-name" {
  default = "eks-demo"
  type    = string
}

variable "aws_profile" {
  default = "default"
  type    = string
}

