#!/bin/bash
#
# This simple shell script build, tag and push custom Jenkins container

VERSION=$1

docker build -t hieunc/jenkins_docker:$VERSION .
docker tag hieunc/jenkins_docker:$VERSION heiunc/jenkins_docker:$VERSION
docker push hieunc/jenkins_docker:$VERSION